package com.toymobi.framework.raw;

import java.io.IOException;
import java.io.InputStream;

import android.content.res.Resources;

import androidx.annotation.NonNull;

import com.toymobi.framework.debug.DebugUtility;

public class GetTextRawFile {

    public static CharSequence getTextRawFile(final Resources res, final int resId) {

        CharSequence charSequence = null;

        if (res != null && resId > 0) {

            try {

                final InputStream raw = res.openRawResource(resId);

                if (raw.available() > 0) {
                    final byte[] reader = new byte[raw.available()];

                    while (raw.read(reader) != -1) {
                        DebugUtility.printDebug("lentidao ao ler ficheiros no raw");
                    }

                    if (reader.length > 0) {
                        charSequence = new String(reader);
                    }
                    raw.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return charSequence;
    }

    private static final String breakLine = "\n";
    private static final String PACKAGE_START_STRING = "android.resource://";
    private static final String PACKAGE_RAW_STRING = "/raw/";

    public static String getRawSoundsFileFixedPathPackage(@NonNull String packageName,
                                                          @NonNull final String rawFilesPath) {

        return PACKAGE_START_STRING + packageName + PACKAGE_RAW_STRING + rawFilesPath;
    }
}
