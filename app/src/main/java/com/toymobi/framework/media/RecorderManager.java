package com.toymobi.framework.media;

import java.io.File;
import java.io.IOException;

import com.toymobi.framework.debug.DebugUtility;

import android.content.Context;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaRecorder.OnInfoListener;

public class RecorderManager {

    public static final int MAX_DURATION = 50000;
    public static final int MAX_FILE_SIZE = 5000000;

    public final int maxDuration = MAX_DURATION;
    public final int maxFileSize = MAX_FILE_SIZE;

    private MediaRecorder audioRecorder = null;
    private MediaPlayer mediaPlayer = null;
    private final String mediaPath;

    public boolean startedRecorder = false;
    public boolean isPlayed = false;

    // public boolean hasMediaFile = false;

    public RecorderManager(final String mediaPath, final Context context) {

        this.mediaPath = mediaPath;

        boolean hasMicrophone = hasMicrophone(context);

        if (hasMicrophone && audioRecorder == null) {
            audioRecorder = new MediaRecorder();
        }

        if (hasMicrophone && mediaPlayer == null) {
            mediaPlayer = new MediaPlayer();
        }

        // hasMediaFile = hasMediaFile(context, mediaPath);
    }

    public final void startRecorder() {
        if (audioRecorder != null && !startedRecorder && mediaPath != null
                && mediaPath.length() > 0) {
            try {
                startedRecorder = true;

                audioRecorder.reset();

                audioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);

                audioRecorder
                        .setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);

                audioRecorder.setOutputFile(mediaPath);

                audioRecorder
                        .setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);

                audioRecorder.setAudioEncodingBitRate(16);

                audioRecorder.setAudioSamplingRate(44100);

                audioRecorder.setMaxDuration(maxDuration);

                audioRecorder.setMaxFileSize(maxFileSize);

                audioRecorder.prepare();

            } catch (IOException e) {
                startedRecorder = false;
                DebugUtility
                        .printException(" RecorderManager startRecorder prepare() failed");
            }
            audioRecorder.start();
            // hasMediaFile = true;
            isPlayed = false;
        } else {
            stopRecorder();
        }
    }

    public final void startRecorder(final OnRecorderListener recorderListener) {
        if (audioRecorder != null && !startedRecorder && mediaPath != null
                && mediaPath.length() > 0) {
            try {
                startedRecorder = true;

                audioRecorder.reset();

                audioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);

                audioRecorder
                        .setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);

                audioRecorder.setOutputFile(mediaPath);

                audioRecorder
                        .setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);

                audioRecorder.setAudioEncodingBitRate(16);

                audioRecorder.setAudioSamplingRate(44100);

                audioRecorder.setMaxDuration(maxDuration);

                audioRecorder.setMaxFileSize(maxFileSize);

                audioRecorder.prepare();

            } catch (IOException e) {
                startedRecorder = false;
                DebugUtility
                        .printException(" RecorderManager startRecorder prepare() failed");

                if (recorderListener != null) {
                    recorderListener.recorderListener();
                }
            }
            audioRecorder.start();
            // hasMediaFile = true;
            isPlayed = false;

            audioRecorder.setOnInfoListener(new OnInfoListener() {

                @Override
                public void onInfo(MediaRecorder mr, int what, int extra) {
                    if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
                        stopRecorder();

                        if (recorderListener != null) {
                            recorderListener.recorderListener();
                        }
                    }
                }
            });

        } else {
            stopRecorder();
        }
    }

    public final void stopRecorder() {
        if (audioRecorder != null && startedRecorder) {
            audioRecorder.stop();
            startedRecorder = false;
            isPlayed = false;
        }
    }

    public final void playMedia() {
        if (mediaPlayer != null && !isPlayed && mediaPath != null
                && mediaPath.length() > 0) {
            try {
                mediaPlayer.reset();
                mediaPlayer.setDataSource(mediaPath);
                mediaPlayer.prepare();
                mediaPlayer.start();
                isPlayed = true;
            } catch (IOException e) {
                if (DebugUtility.IS_DEBUG) {
                    final CharSequence msg = "TestRecActivity"
                            + "prepare() failed";
                    DebugUtility.printException(msg);
                }
                isPlayed = false;
            }

            mediaPlayer.setOnCompletionListener(new OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    isPlayed = false;
                }
            });
        }
    }

    public final void playMedia(final OnRecorderListener recorderListener) {
        if (mediaPlayer != null && !isPlayed && mediaPath != null
                && mediaPath.length() > 0) {
            try {
                mediaPlayer.reset();
                mediaPlayer.setDataSource(mediaPath);
                mediaPlayer.prepare();
                mediaPlayer.start();
                isPlayed = true;
            } catch (IOException e) {

                if (DebugUtility.IS_DEBUG) {
                    final CharSequence msg = "TestRecActivity"
                            + "prepare() failed";
                    DebugUtility.printException(msg);
                }

                recorderListener.recorderListener();
                isPlayed = false;
            }

            mediaPlayer.setOnCompletionListener(new OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    isPlayed = false;
                    recorderListener.recorderListener();
                }
            });
        }
    }

    public final void stopMedia() {
        if (mediaPlayer != null && isPlayed) {
            mediaPlayer.stop();
            isPlayed = false;
        }
    }

    public final void release() {
        stopMedia();
        stopRecorder();

        if (audioRecorder != null) {
            audioRecorder.release();
            audioRecorder = null;
        }

        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    public final boolean hasMicrophone(final Context context) {
        boolean hasMicrophone = false;

        if (context != null) {
            final PackageManager packageManager = context.getPackageManager();

            if (packageManager != null) {

                if (packageManager
                        .hasSystemFeature(PackageManager.FEATURE_MICROPHONE)) {
                    hasMicrophone = true;
                }
            }
        }
        return hasMicrophone;
    }

    public final boolean hasMediaFile(final Context context,
                                      final String path) {
        boolean hasMediaFile = false;

        if (context != null && path != null && path.length() > 0) {
            File mediaRecorderFile = new File(path);
            if (mediaRecorderFile.exists()) {
                hasMediaFile = true;
            }
        }
        return hasMediaFile;
    }

    public final void deleteRecorderFile(final Context context) {

        if (context != null && mediaPath != null && mediaPath.length() > 0) {

            File mediaRecorderFile = new File(mediaPath);

            if (mediaRecorderFile.exists()) {

                //noinspection ResultOfMethodCallIgnored
                mediaRecorderFile.delete();

                if (mediaRecorderFile.exists()) {

                    try {

                        //noinspection ResultOfMethodCallIgnored
                        mediaRecorderFile.getCanonicalFile().delete();

                        if (mediaRecorderFile.exists()) {
                            context.getApplicationContext().deleteFile(
                                    mediaRecorderFile.getName());
                        }
                    } catch (IOException e) {
                        DebugUtility.printException(e.toString());
                    }
                }
            }
        }
    }
}
