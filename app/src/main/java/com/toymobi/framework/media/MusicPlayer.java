package com.toymobi.framework.media;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioAttributes;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.NonNull;

import com.toymobi.framework.debug.DebugUtility;

public class MusicPlayer implements MediaPlayerControl {

    private final Context context;

    private MediaPlayer mediaPlayer;

    private Uri mediaUri;

    private int resourceRawId;

    private final boolean isLooping;

    public MusicPlayer(@NonNull Context context, final int rawIdMedia, boolean isLooping) {
        this.context = context.getApplicationContext();
        resourceRawId = rawIdMedia;
        this.isLooping = isLooping;
    }

    public MusicPlayer(@NonNull Context context, final String mediaPath, boolean isLooping) {
        this.context = context.getApplicationContext();
        mediaUri = Uri.parse(mediaPath);
        this.isLooping = isLooping;
    }

    @Override
    public void loadMedia(final boolean isPlay) {

        initializeMediaPlayer();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            setDataSourceNewApiNVersion(mediaPlayer);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setDataSourceNewApiVersionLollipop(mediaPlayer);
        } else {
            setDataSourceOldApi(mediaPlayer);
        }


        try {

            mediaPlayer.setLooping(isLooping);

            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer player) {
                    if (isPlay) {
                        mediaPlayer.start();
                    }
                }
            });
            mediaPlayer.prepareAsync();
        } catch (Exception e) {
            DebugUtility.printException(e.toString());
        }
    }

    private void setDataSourceNewApiNVersion(@NonNull MediaPlayer mediaPlayer) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            AssetFileDescriptor assetFileDescriptor = context.getResources().openRawResourceFd(resourceRawId);
            try {
                mediaPlayer.setDataSource(assetFileDescriptor);
            } catch (Exception e) {
                DebugUtility.printException(e.toString());
            }
        }
    }

    private void setDataSourceNewApiVersionLollipop(@NonNull MediaPlayer mediaPlayer) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            final AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build();

            mediaPlayer.setAudioAttributes(attributes);

            DebugUtility.printDebug("MediaPlayer setDataSourceNewApiVersionLollipop: %s" + mediaUri);

            try {
                mediaPlayer.setDataSource(context, mediaUri);
            } catch (Exception e) {
                DebugUtility.printException(e.toString());
            }
        }
    }

    private void setDataSourceOldApi(@NonNull MediaPlayer mediaPlayer) {

        DebugUtility.printDebug("MediaPlayer setDataSourceOldApi: %s" + mediaUri);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            try {
                mediaPlayer.setDataSource(context, mediaUri);
            } catch (Exception e) {
                DebugUtility.printException(e.toString());
            }
        }
    }

    @Override
    public void release() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
            DebugUtility.printDebug("MediaPlayer release");
        }
    }

    @Override
    public boolean isPlaying() {
        if (mediaPlayer != null) {
            return mediaPlayer.isPlaying();
        }
        return false;
    }

    @Override
    public void play() {
        if (mediaPlayer != null && !mediaPlayer.isPlaying()) {
            mediaPlayer.start();
        }
    }

    @Override
    public void reset() {
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            loadMedia(isPlaying());
            DebugUtility.printDebug("MediaPlayer reset");
        }
    }

    @Override
    public void pause() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            DebugUtility.printDebug("MediaPlayer pause");
        }
    }

    private void initializeMediaPlayer() {
        if (mediaPlayer == null) {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    DebugUtility.printDebug("MediaPlayer created");
                }
            });
        }
    }
}
