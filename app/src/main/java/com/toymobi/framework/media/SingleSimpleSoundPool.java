package com.toymobi.framework.media;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.os.Build;

import androidx.annotation.RequiresApi;

public class SingleSimpleSoundPool {
    private static final int MAX_QUALITY = 100;
    private static final float NORMAL_SPEED = 1.0f;
    private static final float VOLUME = 1.0f;
    private static final int VOLUME_NONE = -1;

    private SoundPool soundPool;

    private Context context;

    private int indexSoundPool;

    private int sfxMapInt;

    private boolean loaded = true;

    private AudioManager audioManager;

    public SingleSimpleSoundPool(final Context context, final int sfxID) {

        if (context != null) {
            this.context = context;
            this.sfxMapInt = sfxID;
        }
    }

    private void loadAndPlay() {
        if (audioManager == null) {
            audioManager = (AudioManager) context
                    .getSystemService(Context.AUDIO_SERVICE);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            createNewSoundPool();
        } else {
            //noinspection deprecation
            createOldSoundPool();
        }

        soundPool.setOnLoadCompleteListener(new OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId,
                                       int status) {
                loaded = true;
                playSound();
            }
        });

        /*
         * SoundPool retorna um id apos realizar o loading do resource, sendo
         * que este id eh armazena num mapping para buscar este valor de acordo
         * com o id do resources
         */
        indexSoundPool = soundPool.load(context, sfxMapInt, MAX_QUALITY);
    }

    public void playSound() {
        final boolean check = (soundPool != null && audioManager != null && loaded);

        if (check) {

            final float actualVolume = (float) audioManager
                    .getStreamVolume(AudioManager.STREAM_MUSIC);

            final float maxVolume = (float) audioManager
                    .getStreamMaxVolume(AudioManager.STREAM_MUSIC);

            float streamVolume = actualVolume / maxVolume;

            final float volume = (streamVolume == VOLUME_NONE) ? VOLUME : streamVolume;

            soundPool.stop(indexSoundPool);

            soundPool.play(indexSoundPool, volume, volume, 1, 0, NORMAL_SPEED);
        } else {
            loadAndPlay();
        }
    }

    /**
     * Remove all sfx's
     */
    public void deallocate() {
        if (soundPool != null) {
            soundPool.release();
            soundPool = null;
            loaded = false;
        }
    }

    public void release() {
        if (soundPool != null) {
            soundPool.release();
            soundPool = null;
            loaded = false;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void createNewSoundPool() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (soundPool == null) {
                final AudioAttributes attributes = new AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_GAME)
                        .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                        .build();

                if (attributes != null) {
                    soundPool = new SoundPool.Builder().setAudioAttributes(attributes).build();
                }
            }
        }
    }

    @SuppressWarnings("DeprecatedIsStillUsed")
    @Deprecated
    private void createOldSoundPool() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            if (soundPool == null) {
                soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, MAX_QUALITY);
            }
        }
    }
}
