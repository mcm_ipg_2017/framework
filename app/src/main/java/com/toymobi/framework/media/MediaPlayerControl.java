package com.toymobi.framework.media;

public interface MediaPlayerControl {

    void loadMedia(final boolean isPlay);

    void release();

    boolean isPlaying();

    void play();

    void reset();

    void pause();
}
