package com.toymobi.framework.locale;

import java.util.Locale;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.util.DisplayMetrics;

import androidx.annotation.NonNull;

import com.toymobi.framework.persistence.PersistenceManager;

public class LocaleSetting {

    private static final String DEFAULT_LANGUANGE_CODE = "";

    public static void changeLocale(final Context context,
                                    final Locale locale,
                                    final String languangeKeyPersistence) {

        if (context != null && locale != null) {

            final Resources resources = context.getResources();

            if (resources != null) {

                final DisplayMetrics displayMetrics = resources.getDisplayMetrics();

                if (displayMetrics != null) {

                    Configuration configuration = resources.getConfiguration();

                    if (configuration != null) {

                        final Configuration config = new Configuration();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            config.setLocale(locale);
                        } else {
                            //noinspection deprecation
                            setLocalebyOldAPI(config, locale);
                        }

                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
                            //noinspection deprecation
                            updateConfigurationOldApi(context, config);
                        } else {
                            context.createConfigurationContext(config);
                        }
                    }
                }
            }
        }
    }

    public static String getLocalePersistence(final Context context,
                                              final String languangeKeyPersistence) {

        String language = PersistenceManager.loadSharedPreferencesString(
                context, languangeKeyPersistence, DEFAULT_LANGUANGE_CODE);

        if (language.equals(DEFAULT_LANGUANGE_CODE)) {

            final Locale locale = Locale.getDefault();

            if (locale != null) {
                language = locale.getLanguage();
            }
        }
        return language;
    }

    public static void setLocalePersistence(final Context context,
                                            final String languangeKeyPersistence, final String value) {
        PersistenceManager.saveSharedPreferencesString(context,
                languangeKeyPersistence, value);
    }

    @SuppressWarnings("DeprecatedIsStillUsed")
    @Deprecated
    private static void setLocalebyOldAPI(@NonNull Configuration config,
                                          @NonNull Locale locale) {
        config.locale = locale;
    }

    @SuppressWarnings("DeprecatedIsStillUsed")
    @Deprecated
    private static void updateConfigurationOldApi(@NonNull Context context,
                                                  @NonNull Configuration config) {

        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
    }
}
