package com.toymobi.framework.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;

import com.toymobi.framework.debug.DebugUtility;
import com.toymobi.framework.task.LoadTask;
import com.toymobi.framework.task.OnLoadTaskExecute;
import com.toymobi.framework.task.OnLoadTaskFinish;
import com.toymobi.framework.theme.ResourcesAttributes;
import com.toymobi.framework.view.progressdialog.DialogLoadingBar;

public class SplashScreenActivity extends Activity implements OnLoadTaskFinish, OnLoadTaskExecute {

    public static final String BUNDLE_INFO_SPLASH_VIEW = "BUNDLE_INFO_SPLASH_VIEW";
    public static final String BUNDLE_INFO_SPLASH_CLASS = "BUNDLE_INFO_SPLASH_CLASS";

    private static final int TIME_SPLASH_DELAY = 2000;

    private int layout_view_id;

    private String class_name;

    private DialogLoadingBar dialogLoadingBar;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ResourcesAttributes.loadTheme(SplashScreenActivity.this);

        getBundleInfo();

        if (layout_view_id > 0) {
            setContentView(layout_view_id);
        }
        sleep_time();
    }

    private void getBundleInfo() {
        Bundle bundle = this.getIntent().getExtras();
        if (bundle != null) {
            layout_view_id = bundle.getInt(BUNDLE_INFO_SPLASH_VIEW);
            class_name = bundle.getString(BUNDLE_INFO_SPLASH_CLASS);

            if (DebugUtility.IS_DEBUG) {
                final CharSequence msg = "BUNDLE_INFO_SPLASH_VIEW  -> " + layout_view_id;
                DebugUtility.printDebug(msg);

                final CharSequence msg1 = "BUNDLE_INFO_SPLASH_CLASS  -> " + class_name;

                DebugUtility.printDebug(msg1);
            }
            bundle.clear();
        }
    }

    @Override
    public void onLoadTaskFinish() {

        if (DebugUtility.IS_DEBUG) {
            final CharSequence msg = " " + LoadTask.LOAD_TASK_DEBUG_TAG + "onLoadTaskFinish";
            DebugUtility.printDebug(msg);
        }

        if (class_name != null) {
            try {
                final Class<?> cls = Class.forName(class_name);

                final Intent mainActivityIntent = new Intent(SplashScreenActivity.this, cls);

                SplashScreenActivity.this.startActivity(mainActivityIntent);

            } catch (ClassNotFoundException e) {

                if (DebugUtility.IS_DEBUG) {
                    final CharSequence msgException = "BUNDLE_INFO_SPLASH_VIEW  -> " + e.getLocalizedMessage();
                    DebugUtility.printException(msgException);
                }
            }
        }
    }

    @Override
    public void onLoadTaskExecute() {

    }

    private void sleep_time() {

        dialogLoadingBar = new DialogLoadingBar(this, Color.TRANSPARENT,
                Color.TRANSPARENT);

        dialogLoadingBar.showLoader();

        final LoadTask loadTask = new LoadTask(this, this);

        final CountDownTimer countDownTimer = new CountDownTimer(TIME_SPLASH_DELAY, 5000) {
            @Override
            public void onFinish() {
                loadTask.execute();

                if (dialogLoadingBar != null) {
                    dialogLoadingBar.dismissLoder();
                }
            }

            @Override
            public void onTick(final long millisUntilFinished) {
            }
        };
        countDownTimer.start();
    }
}