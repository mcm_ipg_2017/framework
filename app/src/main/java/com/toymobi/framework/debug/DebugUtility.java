package com.toymobi.framework.debug;

import com.toymobi.framework.BuildConfig;

import android.util.Log;

public class DebugUtility {

    public static final boolean IS_DEBUG = BuildConfig.DEBUG;

    public static void debugTest() {
        if (IS_DEBUG) {
            Log.i(DebugUtilityTAG.DEBUG, DebugUtilityTAG.DEBUG);
        } else {
            Log.i(DebugUtilityTAG.DEBUG, DebugUtilityTAG.RELEASE);
        }
    }

    public static void printDebug(final CharSequence msg) {
        if (msg != null) {
            Log.i(DebugUtilityTAG.DEBUG, String.valueOf(msg));
        }
    }

    public static void printDebug(final CharSequence tag, final CharSequence msg) {
        if (tag != null && msg != null) {
            Log.i(String.valueOf(tag), String.valueOf(msg));
        }
    }

    public static void printException(final CharSequence msg) {
        if (msg != null) {
            Log.e(DebugUtilityTAG.EXCEPTION, String.valueOf(msg));
        }
    }

    public static void printException(final CharSequence tag, final CharSequence msg) {
        if (tag != null && msg != null) {
            Log.e(String.valueOf(tag), String.valueOf(msg));
        }
    }
}