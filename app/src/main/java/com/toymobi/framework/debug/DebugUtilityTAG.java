package com.toymobi.framework.debug;

public class DebugUtilityTAG {
    public static final String DEBUG = "DEBUG";
    public static final String RELEASE = "RELEASE";
    public static final String EXCEPTION = "EXCEPTION";
    public static final String MAIN_SCREEN = "MAIN_SCREEN";
    public static final String SHARED_PREFERENCES = "SHARED_PREFERENCES";
    public static final String IMAGE_UTILS = "IMAGE_UTILS";
    public static final String FACEBOOK_DEBUG = "FACEBOOK_DEBUG";
}
