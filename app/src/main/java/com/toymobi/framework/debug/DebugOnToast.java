package com.toymobi.framework.debug;

import android.content.Context;
import android.widget.Toast;

public class DebugOnToast {

    public static void printOnToast (final CharSequence text,
            final Context context) {

        if (text != null && context != null && DebugUtility.IS_DEBUG) {
            Toast.makeText(context, text, Toast.LENGTH_LONG).show();
        }
    }
}
