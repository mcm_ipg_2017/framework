package com.toymobi.framework.debug;

import com.toymobi.framework.theme.ScreenUtilsThemes;

import android.app.Activity;
import android.content.Context;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DebugOnScreen {

    public static void printOnScreen(final CharSequence text, final Context context) {

        if (text != null && context != null) {

            final Activity currentActivity = ((Activity) context);


            final TextView textView = new TextView(context);

            textView.setText(text);

            final LayoutParams param = new LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);

            currentActivity.addContentView(textView, param);
        }
    }

    public static void debugPrintScreenInfo(final Context context) {
        if (context != null) {

            final CharSequence line_separator = System
                    .getProperty("line.separator");

            final CharSequence displayWidth = "displayWidth: "
                    + ScreenUtilsThemes.displayWidth + line_separator;

            final CharSequence displayHeight = "displayHeight i: "
                    + ScreenUtilsThemes.displayHeight + line_separator;

            final CharSequence density = "density: "
                    + ScreenUtilsThemes.density + line_separator;

            final CharSequence display_resolution = "resolution: "
                    + ScreenUtilsThemes.display_resolution + line_separator;

            final CharSequence text = "" + displayWidth + displayHeight
                    + density + display_resolution;

            if (DebugUtility.IS_DEBUG) {
                DebugOnScreen.printOnScreen(text, context);
            }
        }
    }
}