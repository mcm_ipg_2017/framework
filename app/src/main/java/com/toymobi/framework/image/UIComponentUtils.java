package com.toymobi.framework.image;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.view.Gravity;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;

public class UIComponentUtils {
    private static final Bitmap BITMAP_EMPTY = null;

    public static StateListDrawable createStateListDrawable(final Theme theme,
                                                            final int attrOn,
                                                            final int attrOff,
                                                            final int gravity) {

        StateListDrawable stateListDrawable = null;

        if (theme != null && attrOn > 0 && attrOff > 0) {

            stateListDrawable = new StateListDrawable();

            final TypedArray typedArray = theme.obtainStyledAttributes(new int[]{attrOn, attrOff});

            final BitmapDrawable iconToggleOn = (BitmapDrawable) typedArray.getDrawable(0);

            if (iconToggleOn != null) {
                iconToggleOn.setFilterBitmap(true);
                iconToggleOn.setGravity(Gravity.CENTER);
            }

            final BitmapDrawable iconToggleOff = (BitmapDrawable) typedArray.getDrawable(1);

            if (iconToggleOff != null) {
                iconToggleOff.setFilterBitmap(true);
                iconToggleOff.setGravity(gravity);
            }

            stateListDrawable.addState(new int[]{android.R.attr.state_checked}, iconToggleOn);

            stateListDrawable.addState(new int[]{}, iconToggleOff);

            typedArray.recycle();

        }

        return stateListDrawable;
    }

    public static Drawable getDrawableByImageId(final Theme theme,
                                                final int res_image_id) {

        Drawable drawable = null;

        if (theme != null && res_image_id > 0) {

            final TypedArray typedArray = theme.obtainStyledAttributes(new int[]{res_image_id});

            if (typedArray != null) {
                drawable = typedArray.getDrawable(0);
                typedArray.recycle();
            }
        }

        return drawable;
    }

    public static Bitmap getBitmapById(@NonNull final Context context,
                                       final int image_res_id) {

        Drawable drawable = ResourcesCompat.getDrawable(context.getResources(), image_res_id, null);

        return ((drawable != null) ? ((BitmapDrawable) drawable).getBitmap() : null);
    }

    public static int getDimensionPixelSizeByTheme(final Theme theme,
                                                   final int attrValue,
                                                   final int defaultValue) {

        int value = 0;

        if (theme != null && attrValue > 0) {

            final TypedArray typedArray = theme.obtainStyledAttributes(new int[]{attrValue});

            if (typedArray != null) {
                value = typedArray.getDimensionPixelSize(0, defaultValue);
                typedArray.recycle();
            }
        }
        return value;
    }

    public static Bitmap resizeBitmapByDrawableId(final Context context,
                                                  final int new_width,
                                                  final int new_height,
                                                  final int resIdTheme) {

        if (context != null && new_width > 0 && new_height > 0
                && resIdTheme > 0) {

            final Bitmap source = UIComponentUtils.getBitmapById(context,
                    resIdTheme);

            if (source != null) {
                final Matrix matrix = new Matrix();

                final int width = source.getWidth();
                final int height = source.getHeight();
                final float scaleWidth = ((float) new_width) / width;
                final float scaleHeight = ((float) new_height) / height;
                matrix.postScale(scaleWidth, scaleHeight);

                return Bitmap.createBitmap(source, 0, 0, width, height, matrix, true);
            }
        }
        return BITMAP_EMPTY;
    }

    public static Bitmap resizeBitmapById(final Context context,
                                          final int new_width,
                                          final int new_height,
                                          int image_res_id) {

        if (context != null && new_width > 0 && new_height > 0 && image_res_id != -1) {

            final Bitmap source = UIComponentUtils.getBitmapById(context, image_res_id);

            if (source != null) {
                final Matrix matrix = new Matrix();

                final int width = source.getWidth();
                final int height = source.getHeight();
                final float scaleWidth = ((float) new_width) / width;
                final float scaleHeight = ((float) new_height) / height;
                matrix.postScale(scaleWidth, scaleHeight);

                return Bitmap.createBitmap(source, 0, 0, width, height, matrix, true);
            }
        }
        return BITMAP_EMPTY;
    }

    public static Bitmap resizeBitmapById(final Context context,
                                          final int new_height,
                                          int image_res_id) {

        if (context != null && new_height > 0 && image_res_id > 0) {

            final Bitmap source = UIComponentUtils.getBitmapById(context, image_res_id);

            if (source != null) {
                final int oldWidth = source.getWidth();

                final Matrix matrix = new Matrix();

                final int width = source.getWidth();
                final int height = source.getHeight();
                final float scaleWidth = ((float) oldWidth) / width;
                final float scaleHeight = ((float) new_height) / height;
                matrix.postScale(scaleWidth, scaleHeight);

                return Bitmap.createBitmap(source, 0, 0, width, height, matrix, true);

            }
        }
        return BITMAP_EMPTY;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res,
                                                         int resId,
                                                         int reqWidth,
                                                         int reqHeight) {

        if (res != null && resId != -1 && reqWidth > 0 && reqHeight > 0) {

            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();

            options.inJustDecodeBounds = true;

            BitmapFactory.decodeResource(res, resId, options);

            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;

            Bitmap sourceResized = null;

            Bitmap source = BitmapFactory.decodeResource(res, resId, options);

            if (source != null) {
                sourceResized = Bitmap.createScaledBitmap(source, reqWidth, reqHeight, true);

                if (source.isRecycled()) {
                    source.isRecycled();
                }
            }
            return sourceResized;
        } else {
            return null;
        }
    }

    private static int calculateInSampleSize(BitmapFactory.Options options,
                                             int reqWidth,
                                             int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and
            // keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight &&
                    (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }
}