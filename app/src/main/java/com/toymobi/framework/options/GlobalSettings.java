package com.toymobi.framework.options;

public class GlobalSettings {

    public static boolean soundEnable = true;
    public static boolean recorderEnable = false;

    public static final String LANGUAGE_PT_DEFAULT = "pt";
    public static final String LANGUAGE_BR = "pt_BR";
    public static final String LANGUAGE_EN_DEFAULT = "en";

    public static String languageOption = LANGUAGE_PT_DEFAULT;
}
