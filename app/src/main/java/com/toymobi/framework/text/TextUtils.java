package com.toymobi.framework.text;

public class TextUtils {

    public static CharSequence removeCharDuplicatesByOrder(final CharSequence str,
                                                           final char selectLetter) {

        CharSequence word;

        boolean[] seen = new boolean[256];

        StringBuilder sb = new StringBuilder(seen.length);

        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            if (!seen[ch]) {
                if (ch != selectLetter) {
                    seen[ch] = true;
                    sb.append(ch);
                }
            }
        }

        word = sb.toString();

        return word;
    }

    public static CharSequence removeCharDuplicates(final CharSequence str,
                                                    final char selectLetter) {

        CharSequence word;

        int[] charsCount = new int[256];

        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            charsCount[ch]++;
        }

        StringBuilder sb = new StringBuilder(charsCount.length);
        for (int i = 0; i < charsCount.length; i++) {
            if (charsCount[i] > 0 && charsCount[i] != selectLetter) {
                sb.append((char) i);
            }
        }
        word = sb.toString();

        return word;
    }
}