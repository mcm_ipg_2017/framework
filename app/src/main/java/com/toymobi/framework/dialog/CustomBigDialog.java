package com.toymobi.framework.dialog;

import com.toymobi.framework.R;
import com.toymobi.framework.view.textview.JustifyTextViewCustomFont;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager;

public class CustomBigDialog {

    private static final int INVALID_RESOURCES_ID = -1;

    public static void openDialogInformationJustify(final Context context,
                                                    final int layoutId,
                                                    final int textViewDescriptionId,
                                                    final int buttonOkId,
                                                    final CharSequence description) {

        if (context != null && description != null
                && layoutId != INVALID_RESOURCES_ID
                && textViewDescriptionId != INVALID_RESOURCES_ID
                && buttonOkId != INVALID_RESOURCES_ID) {

            final Dialog dialogConfirmation;

            dialogConfirmation = new Dialog(context, R.style.ThemeOverlay_AppCompat_Dialog_Alert);

            dialogConfirmation.requestWindowFeature(Window.FEATURE_NO_TITLE);

            dialogConfirmation.setContentView(layoutId);

            final Window window = dialogConfirmation.getWindow();

            if (window != null) {
                final WindowManager.LayoutParams attributes = window.getAttributes();

                final JustifyTextViewCustomFont dialogDescription = window.findViewById(textViewDescriptionId);

                if (dialogDescription != null) {
                    dialogDescription.setTextJustify(description);
                }

                if (attributes != null) {
                    attributes.windowAnimations = R.style.DialogAnimation;
                }


                final View confirmation_button_yes = window.findViewById(buttonOkId);
                if (confirmation_button_yes != null) {
                    confirmation_button_yes
                            .setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(final View view) {
                                    if (view != null) {
                                        dialogConfirmation.dismiss();
                                    }
                                }
                            });
                }
            }
            dialogConfirmation.show();
        }
    }

    public static void openDialog(final Context context, final int layoutId,
                                  final int buttonOkId, final OnClickListener yes) {

        if (context != null && layoutId != INVALID_RESOURCES_ID
                && buttonOkId != INVALID_RESOURCES_ID) {

            final Dialog dialogConfirmation;

            dialogConfirmation = new Dialog(context, R.style.ThemeOverlay_AppCompat_Dialog_Alert);

            dialogConfirmation.requestWindowFeature(Window.FEATURE_NO_TITLE);

            dialogConfirmation.setContentView(layoutId);

            final Window window = dialogConfirmation.getWindow();

            if (window != null) {
                final WindowManager.LayoutParams attributes = window.getAttributes();


                if (attributes != null) {
                    attributes.windowAnimations = R.style.DialogAnimation;
                }

                final View confirmation_button_yes = window.findViewById(buttonOkId);
                if (confirmation_button_yes != null) {

                    if (yes != null) {
                        confirmation_button_yes
                                .setOnClickListener(new OnClickListener() {
                                    @Override
                                    public void onClick(final View view) {
                                        if (view != null) {
                                            yes.onClick(view);
                                            dialogConfirmation.dismiss();
                                        }
                                    }
                                });
                    } else {
                        confirmation_button_yes
                                .setOnClickListener(new OnClickListener() {
                                    @Override
                                    public void onClick(final View view) {
                                        if (view != null) {
                                            dialogConfirmation.dismiss();
                                        }
                                    }
                                });
                    }

                }
                dialogConfirmation.show();

            }
        }
    }

    public static void openDialogNoDescription(final Context context,
                                               final int layoutId, final int button1Id, final int button2Id,
                                               final OnClickListener button1Listener,
                                               final OnClickListener button2Listener) {

        if (context != null && layoutId != INVALID_RESOURCES_ID
                && button1Id != INVALID_RESOURCES_ID
                && button2Id != INVALID_RESOURCES_ID) {

            final Dialog dialogConfirmation;

            dialogConfirmation = new Dialog(context, R.style.ThemeOverlay_AppCompat_Dialog_Alert);

            dialogConfirmation.requestWindowFeature(Window.FEATURE_NO_TITLE);

            dialogConfirmation.setContentView(layoutId);

            final Window window = dialogConfirmation.getWindow();

            if (window != null) {
                final WindowManager.LayoutParams attributes = window.getAttributes();


                if (attributes != null) {
                    attributes.windowAnimations = R.style.DialogAnimation;
                }

                final View button1 = window.findViewById(button1Id);

                if (button1 != null && button1Listener != null) {
                    button1.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(final View view) {
                            if (view != null) {
                                button1Listener.onClick(view);
                            }
                        }
                    });
                }


                final View button2 = window.findViewById(button2Id);

                if (button2 != null && button2Listener != null) {
                    button2.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(final View view) {
                            if (view != null) {
                                button2Listener.onClick(view);
                                dialogConfirmation.dismiss();
                            }
                        }
                    });
                }

            }

            dialogConfirmation.show();
        }
    }
}
