package com.toymobi.framework.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.toymobi.framework.R;

public class CustomDialog {

    private static final int NO_RES = -1;

    public static void openDialogConfirmation(final Context context,
                                              final int dialogResID,
                                              final CharSequence description,
                                              final OnClickListener no,
                                              final OnClickListener yes,
                                              final int titleResID,
                                              final int yesButtonResID,
                                              final int noButtonResID) {

        final Dialog dialogConfirmation;

        if (context != null && description != null) {

            dialogConfirmation = new Dialog(context);

            dialogConfirmation.requestWindowFeature(Window.FEATURE_NO_TITLE);

            dialogConfirmation.setContentView(dialogResID);

            final Window window = dialogConfirmation.getWindow();

            if (window != null) {

                final View viewDialogLayout = window.findViewById(titleResID);

                if (viewDialogLayout != null) {
                    final TextView dialogTitle = (TextView) viewDialogLayout;
                    dialogTitle.setText(description);
                }

                final WindowManager.LayoutParams attributes = window.getAttributes();

                if (attributes != null) {
                    attributes.windowAnimations = R.style.DialogAnimation;
                }


                final View confirmation_button_yes = window.findViewById(yesButtonResID);

                if (confirmation_button_yes != null) {
                    if (yes != null) {
                        confirmation_button_yes.setOnClickListener(yes);
                        confirmation_button_yes
                                .setOnClickListener(new OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialogConfirmation.dismiss();
                                        yes.onClick(v);
                                    }
                                });
                    } else {
                        confirmation_button_yes
                                .setOnClickListener(new OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialogConfirmation.dismiss();
                                    }
                                });
                    }
                }


                final View confirmation_button_no = window.findViewById(noButtonResID);

                if (confirmation_button_no != null) {

                    if (no != null) {
                        confirmation_button_no.setOnClickListener(no);
                        confirmation_button_no
                                .setOnClickListener(new OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialogConfirmation.dismiss();
                                    }
                                });

                    } else {
                        confirmation_button_no
                                .setOnClickListener(new OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialogConfirmation.dismiss();
                                    }
                                });
                    }
                }
                dialogConfirmation.show();
            }
        }
    }

    public static void openDialogConfirmation(final Context context,
                                              final int dialogResID,
                                              final OnClickListener yes,
                                              final OnClickListener no,
                                              final int yesButtonResID,
                                              final int noButtonResID) {

        final Dialog dialogConfirmation;

        if (context != null) {

            dialogConfirmation = new Dialog(context);

            dialogConfirmation
                    .requestWindowFeature(Window.FEATURE_NO_TITLE);

            dialogConfirmation.setContentView(dialogResID);


            final Window window = dialogConfirmation.getWindow();

            if (window != null) {

                final WindowManager.LayoutParams attributes = window.getAttributes();

                if (attributes != null) {
                    attributes.windowAnimations = R.style.DialogAnimation;

                }

                final View confirmation_button_yes = window.findViewById(yesButtonResID);
                if (confirmation_button_yes != null) {

                    if (yes != null) {
                        confirmation_button_yes
                                .setOnClickListener(new OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialogConfirmation.dismiss();
                                        yes.onClick(v);
                                    }
                                });
                    } else {
                        confirmation_button_yes
                                .setOnClickListener(new OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialogConfirmation.dismiss();
                                    }
                                });
                    }
                }


                final View confirmation_button_no = window.findViewById(noButtonResID);

                if (confirmation_button_no != null) {

                    if (no != null) {

                        confirmation_button_no.setOnClickListener(no);

                        confirmation_button_no
                                .setOnClickListener(new OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialogConfirmation.dismiss();
                                        no.onClick(v);
                                    }
                                });

                    } else {

                        confirmation_button_no
                                .setOnClickListener(new OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialogConfirmation.dismiss();
                                    }
                                });
                    }
                }
                dialogConfirmation.show();
            }
        }
    }

    public static void openDialogConfirmation(final Context context,
                                              final int dialogResID,
                                              final OnClickListener yes,
                                              final OnClickListener no,
                                              final int yesButtonResID,
                                              final int noButtonResID,
                                              final CharSequence title,
                                              final int titleResID) {

        final Dialog dialogConfirmation;

        if (context != null) {

            dialogConfirmation = new Dialog(context);

            dialogConfirmation.requestWindowFeature(Window.FEATURE_NO_TITLE);

            dialogConfirmation.setContentView(dialogResID);

            final Window window = dialogConfirmation.getWindow();

            if (window != null) {

                final WindowManager.LayoutParams attributes = window.getAttributes();

                if (attributes != null) {
                    attributes.windowAnimations = R.style.DialogAnimation;
                }

                if (title != null && titleResID != NO_RES) {
                    final TextView titleDialog = window.findViewById(titleResID);
                    titleDialog.setText(title);
                }

                final View confirmation_button_yes = window.findViewById(yesButtonResID);
                if (yes != null) {
                    confirmation_button_yes
                            .setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialogConfirmation.dismiss();
                                    yes.onClick(v);
                                }
                            });
                } else {
                    confirmation_button_yes
                            .setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialogConfirmation.dismiss();
                                }
                            });
                }

                final View confirmation_button_no = window.findViewById(noButtonResID);
                if (no != null) {

                    confirmation_button_no.setOnClickListener(no);

                    confirmation_button_no
                            .setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialogConfirmation.dismiss();
                                    no.onClick(v);
                                }
                            });

                } else {

                    confirmation_button_no
                            .setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialogConfirmation.dismiss();
                                }
                            });
                }

                dialogConfirmation.show();
            }
        }
    }

    public static void openDialogInformation(final Context context,
                                             final int dialogResID,
                                             final CharSequence description,
                                             final OnClickListener ok,
                                             final int titleResID,
                                             final int yesButtonResID) {

        final Dialog dialogConfirmation;

        if (context != null && description != null) {
            dialogConfirmation = new Dialog(context);

            dialogConfirmation
                    .requestWindowFeature(Window.FEATURE_NO_TITLE);

            dialogConfirmation.setContentView(dialogResID);

            final Window window = dialogConfirmation.getWindow();

            if (window != null) {

                final WindowManager.LayoutParams attributes = window.getAttributes();

                if (attributes != null) {
                    attributes.windowAnimations = R.style.DialogAnimation;

                }
                final TextView dialogTitle = window.findViewById(titleResID);

                if (dialogTitle != null) {
                    dialogTitle.setText(description);
                }


                final View confirmation_button_yes = window.findViewById(yesButtonResID);
                if (ok != null) {
                    confirmation_button_yes.setOnClickListener(ok);
                    confirmation_button_yes
                            .setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialogConfirmation.dismiss();
                                    ok.onClick(v);
                                }
                            });
                } else {
                    confirmation_button_yes
                            .setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialogConfirmation.dismiss();
                                }
                            });
                }
            }
            dialogConfirmation.show();
        }
    }

    public static void openDialogInformation(final Context context,
                                             final int dialogResID,
                                             final OnClickListener ok,
                                             final int yesButtonResID) {

        final Dialog dialogConfirmation;

        if (context != null) {

            dialogConfirmation = new Dialog(context);

            dialogConfirmation.requestWindowFeature(Window.FEATURE_NO_TITLE);

            dialogConfirmation.setContentView(dialogResID);

            final Window window = dialogConfirmation.getWindow();

            if (window != null) {

                final WindowManager.LayoutParams attributes = window.getAttributes();

                if (attributes != null) {
                    attributes.windowAnimations = R.style.DialogAnimation;
                }

                final View confirmation_button_yes = window.findViewById(yesButtonResID);
                if (confirmation_button_yes != null) {

                    if (ok != null) {
                        confirmation_button_yes.setOnClickListener(ok);
                        confirmation_button_yes
                                .setOnClickListener(new OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialogConfirmation.dismiss();
                                        ok.onClick(v);
                                    }
                                });
                    } else {
                        confirmation_button_yes
                                .setOnClickListener(new OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialogConfirmation.dismiss();
                                    }
                                });
                    }
                }
                dialogConfirmation.show();
            }
        }
    }

    public static void openDialogInformation(final Context context,
                                             final int dialogResID,
                                             final CharSequence description,
                                             final int titleResID,
                                             final int yesButtonResID) {

        final Dialog dialogConfirmation;

        if (context != null && description != null) {

            dialogConfirmation = new Dialog(context);

            dialogConfirmation.requestWindowFeature(Window.FEATURE_NO_TITLE);

            dialogConfirmation.setContentView(dialogResID);

            final Window window = dialogConfirmation.getWindow();

            if (window != null) {

                final WindowManager.LayoutParams attributes = window.getAttributes();

                final TextView dialogTitle = window.findViewById(titleResID);

                if (attributes != null) {
                    attributes.windowAnimations = R.style.DialogAnimation;
                }

                dialogTitle.setText(description);

                final View confirmation_button_yes = window.findViewById(yesButtonResID);

                if (confirmation_button_yes != null) {
                    confirmation_button_yes
                            .setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialogConfirmation.dismiss();
                                }
                            });
                }
                dialogConfirmation.show();
            }
        }
    }

    public static void openDialogInformation(final Context context,
                                             final int dialogResID,
                                             final CharSequence description,
                                             final int titleResID,
                                             final int yesButtonResID,
                                             final OnClickListener ok) {

        final Dialog dialogConfirmation;

        if (context != null && description != null) {

            dialogConfirmation = new Dialog(context);

            dialogConfirmation.requestWindowFeature(Window.FEATURE_NO_TITLE);

            dialogConfirmation.setContentView(dialogResID);

            final Window window = dialogConfirmation.getWindow();

            if (window != null) {

                final WindowManager.LayoutParams attributes = window.getAttributes();

                if (attributes != null) {
                    attributes.windowAnimations = R.style.DialogAnimation;
                }

                final TextView dialogTitle = window.findViewById(titleResID);
                if (dialogTitle != null) {
                    dialogTitle.setText(description);
                }

                final View confirmation_button_yes = window.findViewById(yesButtonResID);

                if (confirmation_button_yes != null) {

                    if (ok != null) {
                        confirmation_button_yes.setOnClickListener(ok);

                        confirmation_button_yes
                                .setOnClickListener(new OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialogConfirmation.dismiss();
                                        // ok.onClick(v);
                                    }
                                });

                        dialogConfirmation
                                .setOnDismissListener(new OnDismissListener() {

                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        ok.onClick(confirmation_button_yes);
                                    }
                                });
                    }
                }
                dialogConfirmation.show();
            }
        }
    }
}
