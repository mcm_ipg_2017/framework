package com.toymobi.framework.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;


public class CustomDialogFragment extends DialogFragment {

    public static final int NO_ICON = -1;

    private static final String TITLE_ARGS = "TITLE_ARGS";
    private static final String POSITIVE_ARGS = "POSITIVE_ARGS";
    private static final String NEGATIVE_ARGS = "NEGATIVE_ARGS";
    private static final String OK = "OK";
    private static final String CANCEL = "CANCEL";

    private static DialogInterface.OnClickListener positive_listener;

    private static DialogInterface.OnClickListener negative_listener;

    private static int image_res_id = NO_ICON;

    public static CustomDialogFragment newInstance(final String title,
                                                   final int iconResID,
                                                   final DialogInterface.OnClickListener positive,
                                                   final DialogInterface.OnClickListener negative) {

        image_res_id = iconResID;
        positive_listener = positive;
        negative_listener = negative;

        final CustomDialogFragment customDialogFragment = new CustomDialogFragment();

        final Bundle args = new Bundle();

        args.putString(TITLE_ARGS, title);

        customDialogFragment.setArguments(args);

        return customDialogFragment;
    }

    public static CustomDialogFragment newInstance(final String title,
                                                   final String positive_text, final String negative_text,
                                                   final DialogInterface.OnClickListener positive,
                                                   final DialogInterface.OnClickListener negative) {

        positive_listener = positive;
        negative_listener = negative;

        final CustomDialogFragment customDialogFragment = new CustomDialogFragment();

        final Bundle args = new Bundle();

        args.putString(TITLE_ARGS, title);
        args.putString(POSITIVE_ARGS, positive_text);
        args.putString(NEGATIVE_ARGS, negative_text);

        customDialogFragment.setArguments(args);

        return customDialogFragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        Bundle args = getArguments();

        if (args != null) {

            final String title = args.getString(TITLE_ARGS);
            final String positive_text = args.getString(POSITIVE_ARGS);
            final String negative_text = args.getString(NEGATIVE_ARGS);


            if (image_res_id > NO_ICON) {
                return new AlertDialog.Builder(getActivity())
                        .setIcon(image_res_id)
                        .setTitle(title)
                        .setPositiveButton(
                                ((positive_text == null) ? OK : positive_text),
                                positive_listener)
                        .setNegativeButton(
                                ((negative_text == null) ? CANCEL : negative_text),
                                negative_listener).create();
            } else {
                return new AlertDialog.Builder(getActivity())
                        .setTitle(title)
                        .setPositiveButton(
                                ((positive_text == null) ? OK : positive_text),
                                positive_listener)
                        .setNegativeButton(
                                ((negative_text == null) ? CANCEL : negative_text),
                                negative_listener).create();
            }
        }
        return new AlertDialog.Builder(getActivity()).create();
    }
}
