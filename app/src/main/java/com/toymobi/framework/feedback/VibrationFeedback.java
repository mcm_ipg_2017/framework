package com.toymobi.framework.feedback;

import com.toymobi.framework.debug.DebugUtility;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;

import androidx.annotation.RequiresApi;

public class VibrationFeedback {

    private Vibrator vibrator;
    private static final long VIBRATE_MILLISECONDS_DEFAULT = 100;

    public VibrationFeedback(final Context context) {
        if (context != null) {
            if (vibrator == null) {

                if (checkVibratePermission(context)) {
                    vibrator = (Vibrator) context
                            .getSystemService(Context.VIBRATOR_SERVICE);
                } else {
                    if (DebugUtility.IS_DEBUG) {
                        final CharSequence msg = "VibrationFeedback"
                                + "VIBRATOR_SERVICE Permission Manifest";
                        DebugUtility.printDebug(msg);
                    }
                }
            }
        }
    }

    private boolean checkVibratePermission(final Context context) {
        boolean result = false;
        if (context != null) {
            final String permission = "android.permission.VIBRATE";
            final int res = context.checkCallingOrSelfPermission(permission);

            result = (res == PackageManager.PERMISSION_GRANTED);
        }
        return result;
    }

    public final void vibrate(final long milliseconds) {
        if (vibrator != null && milliseconds > 0) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                vibrateNewAPi(milliseconds);
            } else {
                //noinspection deprecation
                vibrateOldAPi(milliseconds);
            }
        }
    }

    public final void vibrate() {
        if (vibrator != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                vibrateNewAPi();
            } else {
                //noinspection deprecation
                vibrateOldAPi();
            }
        }
    }

    @SuppressWarnings("DeprecatedIsStillUsed")
    @Deprecated
    private void vibrateOldAPi() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            vibrator.vibrate(VIBRATE_MILLISECONDS_DEFAULT);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void vibrateNewAPi() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            final VibrationEffect effect = VibrationEffect.createOneShot(VIBRATE_MILLISECONDS_DEFAULT,
                    VibrationEffect.DEFAULT_AMPLITUDE);
            vibrator.vibrate(effect);
        }
    }


    @SuppressWarnings("DeprecatedIsStillUsed")
    @Deprecated
    private void vibrateOldAPi(final long milliseconds) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            vibrator.vibrate(milliseconds);
        }


    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void vibrateNewAPi(final long milliseconds) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            final VibrationEffect effect = VibrationEffect.createOneShot(milliseconds,
                    VibrationEffect.DEFAULT_AMPLITUDE);
            vibrator.vibrate(effect);
        }
    }
}
