package com.toymobi.framework.view.imageview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.toymobi.framework.R;

@SuppressLint("AppCompatCustomView")
public class ImageViewAutomaticAnimation extends ImageView {

    private Animation automaticAnimation;

    public ImageViewAutomaticAnimation(final Context context) {
        super(context);
    }

    public ImageViewAutomaticAnimation(final Context context,
                                       final AttributeSet attrs) {
        super(context, attrs);
        createAnimation(attrs);
    }

    public ImageViewAutomaticAnimation(final Context context,
                                       final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
        createAnimation(attrs);
    }

    private void createAnimation(final AttributeSet attrs) {
        if (!isInEditMode()) {
            if (automaticAnimation == null) {

                @SuppressLint("CustomViewStyleable") final TypedArray typedArray = getContext()
                        .obtainStyledAttributes(attrs,
                                R.styleable.imageview_animation);

                if (typedArray != null) {
                    final int reference = typedArray.getResourceId(
                            R.styleable.imageview_animation_layout_animation, 0);

                    if (reference != 0) {
                        automaticAnimation = AnimationUtils.loadAnimation(
                                getContext(), reference);
                        if (automaticAnimation != null) {
                            this.startAnimation(automaticAnimation);
                        }
                    }
                    typedArray.recycle();
                }
            }
        }
    }
}
