package com.toymobi.framework.view.textview;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

public class TextViewWrapper extends AppCompatTextView {

    final static int INVALID_RES_ID = -1;

    public TextViewWrapper(final Context context) {
        super(context);
    }

    public TextViewWrapper(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    public TextViewWrapper(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setTextCustom(CharSequence text) {
    }

    public void setTextCustom(int textRawId) {
    }
}
