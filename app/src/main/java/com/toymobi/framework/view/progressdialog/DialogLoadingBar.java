package com.toymobi.framework.view.progressdialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.toymobi.framework.R;

public class DialogLoadingBar extends ProgressDialog {

    private Dialog dialog;

    public DialogLoadingBar(final Context context) {
        super(context);

        if (dialog == null) {
            dialog = new Dialog(context, R.style.Theme_Transparent);
        }
        dialog.setContentView(R.layout.dialog_progress);

        dialog.setCancelable(false);
    }

    public DialogLoadingBar(final Context context, final int layout_id) {
        super(context);

        if (dialog == null) {
            dialog = new Dialog(context, R.style.Theme_Transparent);
        }

        if (layout_id > 0) {
            dialog.setContentView(layout_id);
        } else {
            dialog.setContentView(R.layout.dialog_progress);
        }
        dialog.setCancelable(false);
    }

    public DialogLoadingBar(final Context context, final int background_color,
                            final int background_board_color) {
        super(context);

        if (dialog == null) {
            dialog = new Dialog(context, R.style.Theme_Transparent);
        }
        dialog.setContentView(R.layout.dialog_progress);

        final LinearLayout progressbarLayout = dialog.findViewById(R.id.progressbarLayout);

        if (progressbarLayout != null) {
            progressbarLayout.setBackgroundColor(background_board_color);

            final ProgressBar progressbarLoading = progressbarLayout
                    .findViewById(R.id.progressbarLoading);

            if (progressbarLoading != null) {
                progressbarLoading.setBackgroundColor(background_color);
            }
        }
        dialog.setCancelable(false);
    }

    public DialogLoadingBar(final Context context, final int background_color,
                            final int background_board_color, final int layout_id) {
        super(context);

        if (dialog == null) {
            dialog = new Dialog(context, R.style.Theme_Transparent);
        }

        if (layout_id > 0) {
            dialog.setContentView(layout_id);
        } else {
            dialog.setContentView(R.layout.dialog_progress);
        }

        final LinearLayout progressbarLayout = dialog
                .findViewById(R.id.progressbarLayout);

        if (progressbarLayout != null) {
            progressbarLayout.setBackgroundColor(background_board_color);

            final ProgressBar progressbarLoading = progressbarLayout
                    .findViewById(R.id.progressbarLoading);

            if (progressbarLoading != null) {
                progressbarLoading.setBackgroundColor(background_color);
            }
        }
        dialog.setCancelable(false);
    }

    public void showLoader() {
        if (dialog != null) {
            dialog.show();
        }
    }

    public void dismissLoder() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }
}
