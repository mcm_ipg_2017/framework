package com.toymobi.framework.view.scrollview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

/**
 * This scroll view can be locked to stop the scrolling if some other view wants
 * to capture the touch event.
 */
public class LockableScrollView extends ScrollView {

    public LockableScrollView (final Context context) {
        super(context);
    }

    public LockableScrollView (final Context context,
            final AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent (final MotionEvent motionEvent) {
        if (motionEvent.getAction() == MotionEvent.ACTION_MOVE
                && getParent() != null) {
            getParent().requestDisallowInterceptTouchEvent(true);
        }
        return super.onTouchEvent(motionEvent);
    }

    @Override
    public boolean onInterceptTouchEvent (final MotionEvent motionEvent) {
        if (getParent() != null) {
            switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_MOVE:
                getParent().requestDisallowInterceptTouchEvent(true);
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                getParent().requestDisallowInterceptTouchEvent(false);
                break;
            }
        }
        return super.onInterceptTouchEvent(motionEvent);
    }
}