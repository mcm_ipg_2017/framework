package com.toymobi.framework.view.textview;

import com.toymobi.framework.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

@SuppressLint("AppCompatCustomView")
public class TextViewCustomFont extends TextView {

    public TextViewCustomFont(final Context context) {
        super(context);
        init();
    }

    public TextViewCustomFont(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public TextViewCustomFont(final Context context, final AttributeSet attrs,
                              final int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    private void init() {
        if (!isInEditMode()) {
            final Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/default.ttf");
            setTypeface(tf);
        }
    }

    private void init(final AttributeSet attrs) {
        if (!isInEditMode()) {
            @SuppressLint("CustomViewStyleable") final TypedArray typedArray = getContext().obtainStyledAttributes(
                    attrs, R.styleable.custom_text_view_font);

            if (typedArray != null) {

                final Context context = getContext();

                String fontPath = typedArray
                        .getString(R.styleable.custom_text_view_font_font_custom);

                if (fontPath != null) {

                    final Typeface typeface = Typeface.createFromAsset(context.getAssets(), fontPath);

                    if (typeface != null) {
                        setTypeface(typeface);
                    }
                }
                typedArray.recycle();
            }
        }
    }
}
