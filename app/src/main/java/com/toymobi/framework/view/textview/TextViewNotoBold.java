package com.toymobi.framework.view.textview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.toymobi.framework.R;
import com.toymobi.framework.raw.GetTextRawFile;

import static com.toymobi.framework.R.styleable.custom_text_view_font;

public class TextViewNotoBold extends TextViewWrapper {

    private final static String FONT_PATH = "fonts/NotoSans-Bold.ttf";

    public TextViewNotoBold(final Context context) {
        super(context);

        if (!isInEditMode())
            init();
    }

    public TextViewNotoBold(final Context context, final AttributeSet attrs) {
        super(context, attrs);

        if (!isInEditMode())
            init(attrs);
    }

    public TextViewNotoBold(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);

        if (!isInEditMode())
            init(attrs);
    }

    private void init() {
        if (!isInEditMode()) {
            final Typeface tf = Typeface.createFromAsset(getContext().getAssets(), FONT_PATH);
            if (tf != null) {
                setTypeface(tf);
            }
        }
    }

    private void init(AttributeSet attrs) {
        if (!isInEditMode()) {
            final Typeface tf = Typeface.createFromAsset(getContext().getAssets(), FONT_PATH);
            if (tf != null) {
                setTypeface(tf);
            }

            @SuppressLint("CustomViewStyleable") final TypedArray typedArray = getContext().obtainStyledAttributes(attrs, custom_text_view_font);

            if (typedArray != null) {
                final int rawTextId = typedArray.getResourceId(R.styleable.custom_text_view_font_rawTextIdCustomTextView, INVALID_RES_ID);

                if (rawTextId != INVALID_RES_ID) {
                    final CharSequence text = GetTextRawFile.getTextRawFile(getResources(), rawTextId);

                    if (text != null && text.length() > 0) {
                        this.setText(text);
                    }
                }

                typedArray.recycle();
            }
        }
    }

    @Override
    public void setTextCustom(int textRawId) {
        if (!isInEditMode()) {
            CharSequence text = null;

            if (textRawId != INVALID_RES_ID) {
                text = GetTextRawFile.getTextRawFile(getResources(), textRawId);
            }

            if (text != null && text.length() > 0) {
                this.setText(text);
            }
        }
    }

    @Override
    public void setTextCustom(CharSequence text) {
        if (!isInEditMode()) {
            if (text != null && text.length() > 0) {
                this.setText(text);
            }
        }
    }
}
