package com.toymobi.framework.view.textview;

import java.util.ArrayList;
import java.util.List;

import com.toymobi.framework.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.Paint.Align;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.TextView;

@SuppressLint("AppCompatCustomView")
public class JustifyTextViewCustomFont extends TextView {

    private TextPaint textPaint;

    private int lineHeight;

    private int textAreaWidth;

    private int measuredViewHeight, measuredViewWidth;

    private String text;

    private List<String> listTextLine = new ArrayList<>();

    private boolean hasTextBeenDrown = false;

    private int textSize = 15;

    private int spaceLine = 15;

    private static final String CARACTER_FIXED_SPACE = "  ";

    public JustifyTextViewCustomFont(final Context context) {
        super(context);

        createJustifyText();

        init();
    }

    public JustifyTextViewCustomFont(final Context context,
                                     final AttributeSet attrs) {
        super(context, attrs);

        createJustifyText();

        init(attrs);
    }

    public JustifyTextViewCustomFont(final Context context,
                                     final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);

        createJustifyText();

        init(attrs);
    }

    private void init() {
        if (!isInEditMode()) {
            final Typeface tf = Typeface.createFromAsset(getContext()
                    .getAssets(), "fonts/default.ttf");
            textPaint.setTypeface(tf);
        }
    }

    private void init(final AttributeSet attrs) {
        if (!isInEditMode()) {
            @SuppressLint("CustomViewStyleable") final TypedArray typedArray = getContext().
                    obtainStyledAttributes(attrs, R.styleable.custom_text_view_justify);

            if (typedArray != null && textPaint != null) {

                final String fontPath = typedArray.
                        getString(R.styleable.custom_text_view_justify_font_custom_justify);

                // initialize custom font
                if (fontPath != null && fontPath.length() > 0) {
                    final Typeface typeface = Typeface.createFromAsset(
                            getContext().getAssets(), fontPath);

                    if (typeface != null) {
                        textPaint.setTypeface(typeface);
                    }
                }

                // initialize color
                final int colorText = typedArray.getColor(
                        R.styleable.custom_text_view_justify_textColor,
                        Color.BLACK);

                textPaint.setColor(colorText);

                // initialize text size font
                textSize = typedArray.getDimensionPixelSize(
                        R.styleable.custom_text_view_justify_textSize, 15);

                // initialize space of lines
                spaceLine = typedArray.getDimensionPixelSize(
                        R.styleable.custom_text_view_justify_lineSpaceSize, 15);

                // initialize Align
                textPaint.setTextAlign(Align.LEFT);

                typedArray.recycle();
            }
        }
    }

    private void createJustifyText() {
        if (!isInEditMode()) {

            if (textPaint == null) {
                textPaint = new TextPaint(TextPaint.ANTI_ALIAS_FLAG);
            }

            final ViewTreeObserver viewTreeObserver = getViewTreeObserver();

            if (viewTreeObserver != null) {

                viewTreeObserver
                        .addOnGlobalLayoutListener(new OnGlobalLayoutListener() {

                            @Override
                            public void onGlobalLayout() {

                                if (!hasTextBeenDrown) {

                                    hasTextBeenDrown = true;

                                    setTextAreaWidth(getWidth()
                                            - (getPaddingLeft() + getPaddingRight()));

                                    calculateText();
                                }
                            }
                        });
            }
        }
    }

    private void calculateText() {
        textPaint.setTextSize(textSize);
        setLineHeight();
        listTextLine.clear();
        listTextLine = divideOriginalTextToStringLineList();
        setMeasuredDimentions(listTextLine.size(), getLineHeight(), spaceLine);
        measure(getMeasuredViewWidth(), getMeasuredViewHeight());
    }

    @Override
    protected final void onMeasure(final int widthMeasureSpec,
                                   final int heightMeasureSpec) {

        if (getMeasuredViewWidth() > 0) {
            requestLayout();
            setMeasuredDimension(getMeasuredViewWidth(),
                    getMeasuredViewHeight());
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }

        invalidate();
    }

    @Override
    protected final void onDraw(final Canvas canvas) {

        int rowIndex = getPaddingTop();

        int colIndex;

        final Align align = getAlignment();

        if (align != null) {
            if (getAlignment() == Align.RIGHT) {
                colIndex = getPaddingLeft() + getTextAreaWidth();
            } else {
                colIndex = getPaddingLeft();
            }
            for (int i = 0; i < listTextLine.size(); i++) {
                rowIndex += getLineHeight() + spaceLine;

                canvas.drawText(listTextLine.get(i), colIndex, rowIndex,
                        textPaint);
            }
        }
    }

    private List<String> divideOriginalTextToStringLineList() {

        final List<String> listStringLine = new ArrayList<>();

        String line = "";

        float textWidth;

        if (text != null && text.length() > 0) {
            final String[] listParageraphes = text.split("\n");

            for (String listParageraphe : listParageraphes) {
                String[] arrayWords = listParageraphe.split(" ");

                for (int i = 0; i < arrayWords.length; i++) {

                    line += arrayWords[i] + " ";
                    textWidth = textPaint.measureText(line);

                    if (getTextAreaWidth() == textWidth) {

                        listStringLine.add(line);
                        line = "";
                        continue;
                    } else if (getTextAreaWidth() < textWidth) {

                        int lastWordCount = arrayWords[i].length();

                        line = line.substring(0, line.length() - lastWordCount
                                - 1);

                        if (line.trim().length() == 0)
                            continue;

                        line = justifyTextLine(textPaint, line.trim(),
                                getTextAreaWidth());

                        listStringLine.add(line);
                        line = "";
                        i--;
                        continue;
                    }

                    if (i == arrayWords.length - 1) {
                        listStringLine.add(line);
                        line = "";
                    }
                }
            }
        }
        return listStringLine;
    }

    private String justifyTextLine(final TextPaint textPaint,
                                   String lineString,
                                   final int textAreaWidth) {

        int gapIndex = 0;

        float lineWidth = textPaint.measureText(lineString);

        while (lineWidth < textAreaWidth && lineWidth > 0) {

            gapIndex = lineString.indexOf(" ", gapIndex + 2);

            if (gapIndex == -1) {
                gapIndex = 0;
                gapIndex = lineString.indexOf(" ", gapIndex + 1);

                if (gapIndex == -1)
                    return lineString;
            }

            final String s1 = lineString.substring(0, gapIndex) + CARACTER_FIXED_SPACE;

            final int size = lineString.length();

            final String s2 = lineString.substring(gapIndex + 1, size);

            lineString = s1 + s2;

            lineWidth = textPaint.measureText(lineString);
        }

        return lineString;
    }

    private void setLineHeight() {
        final Rect bounds = new Rect();
        final String sampleStr = "123456789123456789123456789123456789123456789";
        textPaint.getTextBounds(sampleStr, 0, sampleStr.length(), bounds);
        setLineHeight(bounds.height());

    }

    public final void setMeasuredDimentions(final int lineListSize,
                                            final int lineHeigth,
                                            final int lineSpace) {

        int mHeight = lineListSize * (lineHeigth + lineSpace) + lineSpace;

        mHeight += getPaddingRight() + getPaddingLeft();

        setMeasuredViewHeight(mHeight);

        setMeasuredViewWidth(getWidth());
    }

    private int getTextAreaWidth() {
        return textAreaWidth;
    }

    private void setTextAreaWidth(final int textAreaWidth) {
        this.textAreaWidth = textAreaWidth;
    }

    public final int getLineHeight() {
        return lineHeight;
    }

    private int getMeasuredViewHeight() {
        return measuredViewHeight;
    }

    private void setMeasuredViewHeight(final int measuredViewHeight) {
        this.measuredViewHeight = measuredViewHeight;
    }

    private int getMeasuredViewWidth() {
        return measuredViewWidth;
    }

    private void setMeasuredViewWidth(final int measuredViewWidth) {
        this.measuredViewWidth = measuredViewWidth;
    }

    public final void setLineHeight(final int lineHeight) {
        this.lineHeight = lineHeight;
    }

    public final void setTextJustify(final CharSequence text) {
        if (text != null && text.length() > 0) {
            this.text = text.toString();
            calculateText();
            invalidate();
        }
    }

    public final void setTextSize(final int unit, float textSize) {
        textSize = TypedValue.applyDimension(unit, textSize, getContext()
                .getResources().getDisplayMetrics());
        setTextSize(textSize);
    }

    public final void setTextSize(final float textSize) {
        textPaint.setTextSize(textSize);
        calculateText();
        invalidate();
    }

    public final void setTextColor(final int textColor) {
        textPaint.setColor(textColor);
        invalidate();
    }

    public final Align getAlignment() {
        if (textPaint != null) {
            return textPaint.getTextAlign();
        }
        return null;
    }

    public final void setAlignment(final Align align) {
        textPaint.setTextAlign(align);
        invalidate();
    }
}