package com.toymobi.framework.view.textview;

import android.content.Context;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;

public class AnimationView {

    private final static long durationmillis = 1000;

    public static void animationView (final Context context, final View view) {
        if (context != null && view != null) {

            final Animation animation = AnimationUtils.loadAnimation(context,
                    android.R.anim.fade_in);

            if (animation != null) {
                animation.reset();
                view.clearAnimation();
                view.startAnimation(animation);
            }
        }
    }

    public static void animationFadeInOut (final View view,
            final int fade_in_duration, final int fade_out_duration) {
        if (view != null) {
            final Animation fadeIn = new AlphaAnimation(0, 1);
            fadeIn.setInterpolator(new DecelerateInterpolator());

            if (fade_in_duration > 0) {
                fadeIn.setDuration(fade_in_duration);
            } else {

                fadeIn.setDuration(durationmillis);
            }

            final Animation fadeOut = new AlphaAnimation(1, 0);
            fadeOut.setInterpolator(new AccelerateInterpolator());

            if (fade_out_duration > 0) {
                fadeOut.setDuration(fade_out_duration);
            } else {
                fadeOut.setDuration(durationmillis);
            }

            AnimationSet animation = new AnimationSet(false);
            animation.addAnimation(fadeIn);
            animation.addAnimation(fadeOut);
            view.setAnimation(animation);
        }
    }
}
