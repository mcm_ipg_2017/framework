package com.toymobi.framework.view.imageview;

import com.toymobi.framework.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

@SuppressLint("AppCompatCustomView")
public class ImageViewAnimationTTS extends ImageView implements OnClickListener {

    private Animation animation;
    private OnClickListener clickListener;
    private String text;
    private TextToSpeech textToSpeech;

    public ImageViewAnimationTTS(final Context context) {
        super(context);
        setOnClickListener(this);
    }

    public ImageViewAnimationTTS(final Context context,
                                 final AttributeSet attrs) {
        super(context, attrs);
        setOnClickListener(this);
        createAnimation(attrs);
    }

    public ImageViewAnimationTTS(final Context context,
                                 final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
        setOnClickListener(this);
        createAnimation(attrs);
    }

    @Override
    public final void setOnClickListener(final OnClickListener onClickListener) {
        if (onClickListener == this) {
            super.setOnClickListener(onClickListener);
        } else {
            clickListener = onClickListener;
        }
    }

    @Override
    public final void onClick(final View view) {
        if (view != null) {
            if (animation != null) {
                view.startAnimation(animation);
            }

            if (textToSpeech != null) {
                if (text != null && text.length() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        speakNewApiTTS();
                    } else {
                        //noinspection deprecation
                        speakOldApiTTS();
                    }
                }
            }
            if (clickListener != null) {
                clickListener.onClick(this);
            }
        }
    }

    private void createAnimation(final AttributeSet attrs) {
        if (!isInEditMode()) {
            if (animation == null) {

                @SuppressLint("CustomViewStyleable") final TypedArray typedArray = getContext()
                        .obtainStyledAttributes(attrs, R.styleable.imageview_animation);

                if (typedArray != null) {
                    final int reference = typedArray.getResourceId(
                            R.styleable.imageview_animation_layout_animation, 0);

                    if (reference != 0) {
                        animation = AnimationUtils.loadAnimation(getContext(),
                                reference);
                    }

                    text = typedArray
                            .getString(R.styleable.imageview_animation_text_animation);

                    typedArray.recycle();
                }
            }
        }
    }

    public void setTextToSpeech(final TextToSpeech textToSpeech) {
        if (textToSpeech != null) {
            this.textToSpeech = textToSpeech;
        }
    }

    public final void setText(final String textTTS) {

        if (textTTS != null && textTTS.length() > 0) {
            text = textTTS;
        }
    }

    @SuppressWarnings("DeprecatedIsStillUsed")
    @Deprecated
    private void speakOldApiTTS() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
        }

    }

    private void speakNewApiTTS() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            final String utteranceId = this.hashCode() + "";
            textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
        }

    }
}
