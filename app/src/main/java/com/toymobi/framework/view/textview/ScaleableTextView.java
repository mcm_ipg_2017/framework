package com.toymobi.framework.view.textview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.OnScaleGestureListener;
import android.view.View;
import android.view.View.OnTouchListener;

import androidx.core.view.GestureDetectorCompat;

public class ScaleableTextView extends TextViewCustomFont implements
		OnTouchListener, OnGestureListener, OnScaleGestureListener,
		OnDoubleTapListener {

	private ScaleGestureDetector scaleDetector;

	private GestureDetectorCompat gestureDetectorCompat;

	private float dip = Float.MIN_VALUE;

	private float textSize;

	private static final int scaleText = 3;

	private static final int scaleTextTap = 4;

	private static final int MIN_TEXT_SIZE_DESCRIPTION = 14;

	private static final int MAX_TEXT_SIZE_DESCRIPTION = 256;

	public ScaleableTextView(final Context context) {
		super(context);
		init(context);
	}

	public ScaleableTextView(final Context context, final AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public ScaleableTextView(final Context context, final AttributeSet attrs,
			final int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	private void init(final Context context) {

		if (!isInEditMode()) {
			if (dip == Float.MIN_VALUE) {
				dip = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1,
						getResources().getDisplayMetrics());
			}
			textSize = this.getTextSize();

			if (scaleDetector == null) {
				scaleDetector = new ScaleGestureDetector(context, this);

			}

			if (gestureDetectorCompat == null) {
				gestureDetectorCompat = new GestureDetectorCompat(context, this);
				gestureDetectorCompat.setOnDoubleTapListener(this);
			}
		}
	}

	@Override
	public boolean onScale(final ScaleGestureDetector detector) {
		final float currentSpan = detector.getCurrentSpan();
		final float previousSpan = detector.getPreviousSpan();

		if (currentSpan > previousSpan && textSize < MAX_TEXT_SIZE_DESCRIPTION) {
			textSize = textSize + (dip * scaleText);
			this.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
		} else if (currentSpan < previousSpan
				&& textSize > MIN_TEXT_SIZE_DESCRIPTION) {
			textSize = textSize - (dip * scaleText);
			this.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
		}
		return true;
	}

	@Override
	public boolean onScaleBegin(final ScaleGestureDetector detector) {
		return true;
	}

	@Override
	public void onScaleEnd(final ScaleGestureDetector detector) {
	}

	@Override
	public boolean onTouch(final View view, final MotionEvent event) {
		if (gestureDetectorCompat != null
				&& gestureDetectorCompat.onTouchEvent(event)
				&& scaleDetector != null && scaleDetector.onTouchEvent(event)) {
			return true;
		}
		return super.onTouchEvent(event);
	}

	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (gestureDetectorCompat != null
				&& gestureDetectorCompat.onTouchEvent(event)
				&& scaleDetector != null && scaleDetector.onTouchEvent(event)) {
			return true;
		}
		return super.onTouchEvent(event);
	}

	@Override
	public boolean onDoubleTap(MotionEvent motionEvent) {
		return true;
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent motionEvent) {
		if (textSize < MAX_TEXT_SIZE_DESCRIPTION) {
			textSize = textSize + (dip * scaleTextTap);
			this.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
			return true;
		}
		return false;
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
		if (textSize > MIN_TEXT_SIZE_DESCRIPTION) {
			textSize = textSize - (dip * scaleTextTap);
			this.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
			return true;
		}
		return false;
	}

	@Override
	public boolean onDown(MotionEvent e) {
		return true;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		return true;
	}

	@Override
	public void onLongPress(MotionEvent e) {

	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		return true;
	}

	@Override
	public void onShowPress(MotionEvent e) {
	}

	@Override
	public boolean onSingleTapUp(MotionEvent motionEvent) {
		return true;
	}
}
