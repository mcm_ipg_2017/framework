package com.toymobi.framework.task;

public interface OnLoadTaskFinish {

    void onLoadTaskFinish();
}
