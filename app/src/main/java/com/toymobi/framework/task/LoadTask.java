package com.toymobi.framework.task;

import com.toymobi.framework.debug.DebugUtility;

import android.os.AsyncTask;

public class LoadTask extends AsyncTask<Void, Void, Void> {
    public static final String LOAD_TASK_DEBUG_TAG = "LoadTask";
    private final OnLoadTaskFinish onLoadTaskFinish;
    private final OnLoadTaskExecute onLoadTaskExecute;

    public LoadTask(final OnLoadTaskFinish onLoadTaskFinish,
                    final OnLoadTaskExecute onLoadTaskExecute) {
        this.onLoadTaskFinish = onLoadTaskFinish;
        this.onLoadTaskExecute = onLoadTaskExecute;
    }

    @Override
    protected void onPreExecute() {
        if (DebugUtility.IS_DEBUG) {
            final CharSequence msg = "onPreExecute";
            DebugUtility.printDebug(LOAD_TASK_DEBUG_TAG, msg);
        }
    }

    @Override
    protected Void doInBackground(final Void... params) {
        if (DebugUtility.IS_DEBUG) {
            final CharSequence msg = "doInBackground";
            DebugUtility.printDebug(LOAD_TASK_DEBUG_TAG, msg);
        }

        if (onLoadTaskExecute != null) {
            onLoadTaskExecute.onLoadTaskExecute();
        }
        return null;
    }

    @Override
    protected void onPostExecute(final Void result) {
        if (DebugUtility.IS_DEBUG) {
            final CharSequence msg = "onPostExecute";
            DebugUtility.printDebug(LOAD_TASK_DEBUG_TAG, msg);
        }

        if (onLoadTaskFinish != null) {
            onLoadTaskFinish.onLoadTaskFinish();
        }
    }
}
