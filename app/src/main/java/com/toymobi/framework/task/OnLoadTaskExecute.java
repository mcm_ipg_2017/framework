package com.toymobi.framework.task;

public interface OnLoadTaskExecute {

    @SuppressWarnings("EmptyMethod")
    void onLoadTaskExecute();
}
