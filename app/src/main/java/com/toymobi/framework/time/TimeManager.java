package com.toymobi.framework.time;

import android.content.Context;

import com.toymobi.framework.persistence.PersistenceManager;

public class TimeManager {

    private long startTime = -1;

    private long estimatedTime = -1;

    private String mini_game_time_tag;
    private Context context;

    public TimeManager(final Context context, final String mini_game_time_tag) {
        if (context != null) {
            this.context = context;
            this.mini_game_time_tag = mini_game_time_tag;
        }
    }

    public void start() {
        startTime = System.currentTimeMillis();
    }

    public boolean is_new_recording = false;

    public void win() {

        estimatedTime = System.currentTimeMillis() - startTime;

        long oldTime = load();

        if (oldTime == -1) {
            save(estimatedTime);
            is_new_recording = false;
        } else {
            is_new_recording = (oldTime > estimatedTime);

            if (is_new_recording) {
                // new record
                save(estimatedTime);
            }
        }
    }

    public final String getMessageWinner(final CharSequence timeMsg,
                                         final CharSequence newRecordtimeMsg) {
        final int seconds = (int) (estimatedTime / 1000) % 60;
        final int minutes = (int) ((estimatedTime / (1000 * 60)) % 60);
        final int hours = (int) ((estimatedTime / (1000 * 60 * 60)) % 24);

        final String checked_hours = ((hours > 0) ? hours + "h: " : "");
        final String checked_minutes = ((minutes > 0) ? minutes + "m: " : "");
        final String checked_seconds = ((seconds > 0) ? seconds + "s " : "");

        final String result = " " + checked_hours + checked_minutes
                + checked_seconds;

        String msg;

        if (is_new_recording) {

            if (newRecordtimeMsg != null) {
                msg = newRecordtimeMsg + result;
            } else {
                msg = "" + result;
            }

        } else {
            if (timeMsg != null) {
                msg = timeMsg + result;
            } else {
                msg = "" + result;
            }
        }
        return msg;
    }

    private void save(final long time) {
        if (time > 0) {
            PersistenceManager.saveSharedPreferencesLong(context,
                    mini_game_time_tag, time);
        }
    }

    private long load() {
        return PersistenceManager.loadSharedPreferencesLong(context,
                mini_game_time_tag, -1);
    }
}
