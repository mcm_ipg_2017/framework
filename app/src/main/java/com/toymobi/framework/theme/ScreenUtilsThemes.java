package com.toymobi.framework.theme;

import com.toymobi.framework.debug.DebugUtility;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.WindowManager;

public class ScreenUtilsThemes {
    public static int displayWidth;
    public static int displayHeight;
    public static float density;

    // private static final int QVGA_WIDTH = 240;
    private static final int HVGA_WIDTH = 320;
    private static final int WVGA_WIDTH = 480;
    private static final int DVGA_WIDTH = 640;

    private static final int DVGA_WIDTH_600 = 600;

    // private static final int QVGA_HEIGTH = 320;
    private static final int HVGA_HEIGTH = 480;
    private static final int WVGA_HEIGTH = 800;
    private static final int DVGA_HEIGTH = 960;

    public static int orientation;

    /* 240x320 */
    public static final CharSequence QVGA = "qvga";

    /* 320x480 */
    public static final CharSequence HVGA = "hvga";

    /* 480x800 */
    public static final CharSequence WVGA = "wvga";

    /* 480x800 */
    public static final CharSequence WVGA_LARGE_DPI = "wvga_large_dpi";

    /* 640x960 */
    public static final CharSequence DVGA = "dvga";

    /* 640x960 */
    public static final CharSequence DVGA_LARGE_DPI = "dvga_large_dpi";

    public static CharSequence display_resolution;

    public static String theme;

    public static final String DEBUG_TAG_SCREEN = "DEBUG_TAG_SCREEN";

    public static void setScreenSize(final Context context) {

        if (context != null) {

            final WindowManager windowManager = (WindowManager)
                    context.getSystemService(Context.WINDOW_SERVICE);

            if (windowManager != null) {

                if (displayWidth == 0 && displayHeight == 0) {

                    final DisplayMetrics displayMetrics = new DisplayMetrics();

                    windowManager.getDefaultDisplay().getMetrics(displayMetrics);

                    density = displayMetrics.density;

                    displayWidth = displayMetrics.widthPixels;

                    displayHeight = displayMetrics.heightPixels;

                    orientation = getOrientation(context);

                    if (DebugUtility.IS_DEBUG) {
                        final CharSequence displayWidthMsg = "displayWidth DP: "
                                + displayWidth;

                        DebugUtility.printDebug(DEBUG_TAG_SCREEN,
                                displayWidthMsg);

                        final CharSequence displayHeightMsg = "displayHeight DP: "
                                + displayHeight;

                        DebugUtility.printDebug(DEBUG_TAG_SCREEN,
                                displayHeightMsg);

                        final CharSequence densityMsg = "density: "
                                + density;

                        DebugUtility.printDebug(DEBUG_TAG_SCREEN,
                                densityMsg);

                        final CharSequence widthPixelsMsg = "widthPixels: "
                                + displayMetrics.widthPixels;

                        DebugUtility.printDebug(DEBUG_TAG_SCREEN,
                                widthPixelsMsg);

                        final CharSequence heightPixelsMsg = "heightPixels: "
                                + displayMetrics.heightPixels;

                        DebugUtility.printDebug(DEBUG_TAG_SCREEN,
                                heightPixelsMsg);

                        final CharSequence densityDpiMsg = "densityDpi: "
                                + displayMetrics.densityDpi;

                        DebugUtility.printDebug(DEBUG_TAG_SCREEN,
                                densityDpiMsg);
                    }
                    checkScreenTheme(displayMetrics);
                }
            }
        }
    }

    private static void checkScreenTheme(final DisplayMetrics displayMetrics) {
        if (displayMetrics.densityDpi >= 320) {
            display_resolution = DVGA_LARGE_DPI;
        } else {
            checkVGA();
        }
    }

    private static void checkVGA() {
        if (displayWidth < HVGA_WIDTH && displayHeight < HVGA_HEIGTH) {
            display_resolution = QVGA;
        } else if (displayWidth < WVGA_WIDTH && displayHeight < WVGA_HEIGTH) {
            display_resolution = HVGA;
        } else if (displayWidth < DVGA_WIDTH && displayHeight < DVGA_HEIGTH) {
            display_resolution = WVGA;
        } else if (displayWidth >= DVGA_WIDTH && displayHeight >= DVGA_HEIGTH) {
            display_resolution = DVGA;
        }
        /*
         * Samsung Galaxy P1000 screen size
         */
        else if (displayWidth >= DVGA_WIDTH_600 && displayHeight >= DVGA_HEIGTH) {
            display_resolution = DVGA;
        }
    }

    private static int getOrientation(final Context context) {
        int orientation = 0;
        if (context != null) {
            orientation = context.getResources().getConfiguration().orientation;
        }
        return orientation;
    }
}
