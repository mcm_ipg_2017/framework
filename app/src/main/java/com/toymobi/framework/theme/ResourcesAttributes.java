package com.toymobi.framework.theme;

import com.toymobi.framework.debug.DebugUtility;

import android.content.Context;
import android.content.res.Resources;

public class ResourcesAttributes {
    public static Resources resources;
    public static Context applicationContext;
    public static final String DEBUG_TAG_SCREEN = "DEBUG_TAG_SCREEN";
    public static String packageName;

    public static final float DISTANCE_DIP = 40.0f;

    public static int MINIMUM_VELOCITY;

    public static String PACKAGE_NAME;

    public static float SCALE;

    public static float SCALE_DISTANCE_DIP;

    public static final int THRESHOLD_ACTION = 120;

    public static int SCALED_VELOCITY_ADJUST = 0;

    public static final String THEME_DEFAULT = "theme_default";

    public static final String STYLE = "style";

    public static void loadTheme(final Context context) {

        if (context != null) {

            if (ScreenUtilsThemes.theme == null) {

                ScreenUtilsThemes.theme = "theme_" + ScreenUtilsThemes.display_resolution;

                if (DebugUtility.IS_DEBUG) {
                    final CharSequence msg = " ScreenUtils.theme : " + ScreenUtilsThemes.theme;
                    DebugUtility.printDebug(DEBUG_TAG_SCREEN, msg);
                }
            }

            if (resources == null) {
                resources = context.getResources();
            }

            if (applicationContext == null) {
                applicationContext = context.getApplicationContext();
            }

            if (packageName == null) {
                packageName = context.getPackageName();
            }

            final int themeId = resources.getIdentifier(ScreenUtilsThemes.theme, STYLE, packageName);

            if (themeId != 0) {
                context.setTheme(themeId);
            }
        }
    }
}