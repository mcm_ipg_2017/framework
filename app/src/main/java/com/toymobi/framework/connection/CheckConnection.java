package com.toymobi.framework.connection;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class CheckConnection {

    public static boolean isNetworkAvailable(final Context context) {

        boolean value = false;

        if (context != null) {

            final ConnectivityManager manager = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);

            if (manager != null) {
                final NetworkInfo info = manager.getActiveNetworkInfo();

                if (info != null && info.isConnected()) {
                    value = true;
                }
            }
        }
        return value;
    }
}
