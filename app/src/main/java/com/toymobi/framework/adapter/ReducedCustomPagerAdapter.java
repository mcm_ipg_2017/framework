package com.toymobi.framework.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.collection.SparseArrayCompat;
import androidx.viewpager.widget.PagerAdapter;

public class ReducedCustomPagerAdapter extends PagerAdapter {

    private SparseArrayCompat<View> pages;

    private final View VIEW_NULL = null;

    public ReducedCustomPagerAdapter(final int count) {

        if (pages == null) {
            pages = new SparseArrayCompat<>(count);
        }
    }

    @Override
    public int getCount() {

        if (pages != null) {
            return pages.size();
        }

        return 0;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public final Object instantiateItem(@NonNull final ViewGroup view, final int position) {

        View viewItem = null;

        if (position >= 0) {
            viewItem = pages.get(position);

            if (viewItem != null) {
                view.addView(viewItem);
            }
        }
        //noinspection ConstantConditions
        return viewItem;
    }

    @Override
    public void destroyItem(@NonNull final ViewGroup view, final int index, @NonNull final Object object) {

        if (index >= 0) {
            view.removeView((View) object);
        }
    }

    public void addViewPage(final int pageNumber, View page) {

        if (pages == null) {
            pages = new SparseArrayCompat<>();
        }
        pages.put(pageNumber, page);
    }

    public void addViewById(@NonNull LayoutInflater layoutInflater, final int idView, final int position, ViewGroup viewGroup) {

        if (idView > 0) {

            final View view = layoutInflater.inflate(idView, viewGroup);

            if (view != null) {
                if (pages == null) {
                    pages = new SparseArrayCompat<>();
                }
                pages.put(position, view);
            }
        }
    }

    public View getViewPage(final int pageNumber) {

        View view = null;

        if (pages != null) {
            view = pages.get(pageNumber);
        }

        return view;
    }

    public void deallocate() {
        if (pages != null) {
            pages.clear();
            pages = null;
        }
    }
}
