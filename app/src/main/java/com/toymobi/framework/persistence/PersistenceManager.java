package com.toymobi.framework.persistence;

import com.toymobi.framework.BuildConfig;
import com.toymobi.framework.debug.DebugUtility;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PersistenceManager {

    private static final String SHARED_PREFERENCES = "PersistenceManager";

    public static void saveSharedPreferencesString(final Context context,
                                                   final String key,
                                                   final String value) {

        if (context != null && key != null && value != null) {

            final SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());

            if (sharedPrefs != null) {

                final SharedPreferences.Editor editor = sharedPrefs.edit();

                if (editor != null) {

                    if (BuildConfig.DEBUG) {
                        final CharSequence msg = "saveSharedPreferencesString -> key: " + key + "  -> value:" + value;
                        printDebug(msg);
                    }

                    editor.putString(key, value);
                    editor.apply();
                }
            }
        }
    }

    public static String loadSharedPreferencesString(final Context context,
                                                     final String key,
                                                     final String defaultValue) {

        String value = defaultValue;

        if (context != null && key != null && defaultValue != null) {

            final SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());

            if (sharedPrefs != null) {
                value = sharedPrefs.getString(key, defaultValue);
            }
        }

        if (BuildConfig.DEBUG) {
            final CharSequence msg = "loadSharedPreferencesString -> key: " + key + "  -> value:" + value;
            printDebug(msg);
        }

        return value;
    }

    public static void saveSharedPreferencesInt(final Context context,
                                                final String key,
                                                final int value) {

        if (context != null && key != null) {

            final SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());

            if (sharedPrefs != null) {
                final SharedPreferences.Editor editor = sharedPrefs.edit();

                if (editor != null) {

                    if (BuildConfig.DEBUG) {
                        final CharSequence msg = "saveSharedPreferencesInt -> key: " + key + "  -> value:" + value;
                        printDebug(msg);
                    }

                    editor.putInt(key, value);
                    editor.apply();
                }
            }
        }
    }

    public static int loadSharedPreferencesInt(final Context context,
                                               final String key,
                                               final int defaulValue) {

        int value = defaulValue;

        if (context != null && key != null) {

            final SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());

            if (sharedPrefs != null) {
                value = sharedPrefs.getInt(key, defaulValue);
            }
        }

        if (BuildConfig.DEBUG) {
            final CharSequence msg = "loadSharedPreferencesInt -> key: " + key + "  -> value:" + value;
            printDebug(msg);
        }

        return value;
    }

    public static void saveSharedPreferencesBoolean(final Context context,
                                                    final String key,
                                                    final boolean value) {

        if (context != null && key != null) {

            final SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());

            if (sharedPrefs != null) {
                final SharedPreferences.Editor editor = sharedPrefs.edit();

                if (editor != null) {

                    if (BuildConfig.DEBUG) {
                        final CharSequence msg = "saveSharedPreferencesBoolean -> key: " + key + "  -> value:" + value;
                        printDebug(msg);
                    }

                    editor.putBoolean(key, value);
                    editor.apply();
                }
            }
        }
    }

    public static boolean loadSharedPreferencesBoolean(final Context context,
                                                       final String key,
                                                       final boolean defaulValue) {

        boolean value = defaulValue;

        if (context != null && key != null) {

            final SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());

            if (sharedPrefs != null) {
                value = sharedPrefs.getBoolean(key, defaulValue);
            }
        }

        if (BuildConfig.DEBUG) {
            final CharSequence msg = "loadSharedPreferencesBoolean -> key: " + key + "  -> value:" + value;
            printDebug(msg);
        }
        return value;
    }

    public static void saveSharedPreferencesLong(final Context context,
                                                 final String key,
                                                 final long value) {

        if (context != null && key != null) {

            final SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());

            if (sharedPrefs != null) {
                final SharedPreferences.Editor editor = sharedPrefs.edit();

                if (editor != null) {

                    if (BuildConfig.DEBUG) {
                        final CharSequence msg = "saveSharedPreferencesLong -> key: " + key + "  -> value:" + value;
                        printDebug(msg);
                    }
                    editor.putLong(key, value);
                    editor.apply();
                }
            }
        }
    }

    public static long loadSharedPreferencesLong(final Context context,
                                                 final String key,
                                                 final long defaulValue) {

        long value = defaulValue;

        if (context != null && key != null) {

            final SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());

            if (sharedPrefs != null) {
                value = sharedPrefs.getLong(key, defaulValue);
            }
        }

        if (BuildConfig.DEBUG) {
            final CharSequence msg = "loadSharedPreferencesLong -> key: " + key + "  -> value:" + value;
            printDebug(msg);
        }

        return value;
    }

    private static void printDebug(final CharSequence msg) {
        if (msg != null) {
            if (DebugUtility.IS_DEBUG) {
                DebugUtility.printDebug(PersistenceManager.SHARED_PREFERENCES, msg);
            }
        }
    }
}